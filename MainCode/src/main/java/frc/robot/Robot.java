package frc.robot;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.LimitSwitchNormal;
import com.ctre.phoenix.motorcontrol.LimitSwitchSource;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.BaseMotorController;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import edu.wpi.first.wpilibj.AnalogInput;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.PWMVictorSPX;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

public class Robot extends TimedRobot {
  private DifferentialDrive m_myrobot;
  private SpeedControllerGroup left;
  private SpeedControllerGroup right;
  private PWMVictorSPX sucker;
  private PWMVictorSPX lights;
  private TalonSRX motorEncoder;

  private CameraServer camserv;
  // The larger the gear reduction the better
  // should stop at 120
  // make sure to have a pressure switch/pressure relief valve
  private Compressor compressor;
  // should be at most 60
  // private DoubleSolenoid dsol1;   private DoubleSolenoid dsol2;

  private UsbCamera _cam;

  private double kballsuccerSoftLimit = 37;
  private double kballsuccerTicksPerInch = 652; //4096 is ticks per revolution need to convert to inches
  private double ballsuccerP = .7;
  private double ballsuccerI = 0;
  private double ballsuccerD = 0;
  private double kballsuccerTolerance = .5;
  private double khome = -1;

  private DigitalInput testLimitSwitch;

  private AnalogInput sonic0;
  private AnalogInput sonic1;


  private XboxController controller;
  private XboxController controller2;
  // Inversion
  // MotorMagic

  /*
  Helpful sources:
  Chief Delphi
  Programming done right frc
  StackOverFlow (ofc)
  Logan Buyers
  Head First Java
  ScreenStepsLive
  */

  // PID loop set f to 0 highly recommended

  @Override
    public void robotInit() {
    sonic0 = new AnalogInput(0);
    sonic1 = new AnalogInput(1);
    testLimitSwitch = new DigitalInput(1);
    camserv = CameraServer.getInstance();
    camserv.startAutomaticCapture(0);
    camserv.startAutomaticCapture(1);
  compressor = new Compressor();
  // dsol1 = new DoubleSolenoid(0, 1);  dsol2 = new DoubleSolenoid(2, 3);

    left = new SpeedControllerGroup(new PWMVictorSPX(7));
    right = new SpeedControllerGroup(new PWMVictorSPX(9));
    sucker = new PWMVictorSPX(8);
    m_myrobot = new DifferentialDrive(left, right);
    lights = new PWMVictorSPX(6);
    motorEncoder = CANTalonFactory.createTalon(1, false, NeutralMode.Brake,
    FeedbackDevice.QuadEncoder, 0, true);
    motorEncoder = CANTalonFactory.setupHardLimits(motorEncoder, LimitSwitchSource.Deactivated,
    LimitSwitchNormal.Disabled, false, LimitSwitchSource.Deactivated, LimitSwitchNormal.Disabled, false);
    motorEncoder = CANTalonFactory.setupSoftLimits(motorEncoder, true, (int) Math.round(kballsuccerSoftLimit*kballsuccerTicksPerInch),
    true, 0);
    motorEncoder = CANTalonFactory.tuneLoops(motorEncoder, 0, ballsuccerP, ballsuccerI, ballsuccerD, 0);
    motorEncoder.setSelectedSensorPosition(0); //ballsuccer ALL THE WAY DOWN ON POWER UP




//plug in green control second!!!!!!!!!
    controller2 = new XboxController(1);
    controller = new XboxController(0);

    System.out.println("Let's Get This Bread");
  
  }

  @Override
  public void teleopInit() {
    Init();
  }

  @Override
  public void disabledInit() {
    compressor.stop();
    setPosition(0);
    motorEncoder.set(ControlMode.Disabled, 0);
  }

  public void Init()
  {
    // compressor.start();
    // sucker.setSafetyEnabled(true);
  }
  private double disToWall = 0;
  private double angToWall = 90;
  public void periodic()
  {
    //dis to wall ang to wall
    disToWall = (sonic0.getValue() + sonic1.getValue())/2;
    angToWall = Math.toDegrees((Math.atan(((sonic1.getValue()-sonic0.getValue())/60)))); //60 = space tween sensors

  


    if(testLimitSwitch.get()){
      //switch is pressed
    }
    else{
      //switch is not pressed
    }

    ballsuccerUpdater();
    // TODO CAN CONVERT
    m_myrobot.tankDrive(-(controller.getY(Hand.kLeft)*Math.abs(controller.getY(Hand.kLeft))*.8), -(controller.getY(Hand.kRight)*Math.abs(controller.getY(Hand.kRight))*.8));

    m_myrobot.feedWatchdog(); 

    if (controller2.getBumper(Hand.kLeft))
    {
      sucker.set(.7);
    }
    else if (controller2.getBumper(Hand.kRight)){
      sucker.set(-.5);
    }
    else{
      sucker.set(0);
    }
if (controller2.getPOV()==90){
  lights.set(0.5);
  }
  // if(controller2.)
    // if (controller.getAButtonPressed())
    // {
    //   dsol1.set(DoubleSolenoid.Value.kReverse);
    // }  
    // if (!controller.getAButtonPressed())
    // {
    //   dsol1.set(DoubleSolenoid.Value.kForward);
    // }
    // // Enable / disable solenoid front
    // if (controller.getBButtonPressed())
    // {
    //   // Enable / disable solenoid back
    //   dsol2.set(DoubleSolenoid.Value.kReverse);
    // }
    // if (!controller.getBButtonPressed())
    // {
    //   dsol2.set(DoubleSolenoid.Value.kForward);
    // }
  }

  @Override
  public void autonomousInit()
  {
    Init();
  }

  @Override
  public void autonomousPeriodic()
  {
    periodic();
  }

  @Override
  public void teleopPeriodic()
  {
    periodic();
  }
  private void ballsuccerUpdater()
  {
    //triggers
    if (controller2.getTriggerAxis(Hand.kRight) > .1)
    {
      jog(controller2.getTriggerAxis(Hand.kRight)*.1);     
    }
    else if (controller2.getTriggerAxis(Hand.kLeft) > .1)
    {
      jog(controller2.getTriggerAxis(Hand.kLeft)*-.1);
    }

    //presets
    //driving
    if (controller2.getXButtonPressed()) {
      setPosition(2);
    }
    //bottom
    if (controller2.getAButtonPressed()){
      setPosition(0.5);
    }
    if (controller2.getBButtonPressed()){
      //middle
      setPosition(27.5);
    }
    if (controller2.getYButtonPressed()){
      //top
      setPosition(39);
    }

    positionUpdater();
  }

   //CLOSED LOOP CONTROL
   private double mWantedPosition = .1;
   private double mTravelingPosition = 0;
   
   public synchronized void setPosition(double pos){
       if(pos>=kballsuccerSoftLimit){
         pos=kballsuccerSoftLimit;
       }
       else if(pos<0) {
         pos=0;
       } 
       mWantedPosition=pos; 
   }

   private boolean jog=false;

   public void jog(double amount){
       mWantedPosition+=amount;
       setPosition(mWantedPosition);
       jog=true;
       //System.out.println("wanted position = " + mWantedPosition);
   }


  public boolean atPosition(){
     if(Math.abs(mWantedPosition-getPosition())<=kballsuccerTolerance){
         return true;
     }
     else{
         return false;
     }
  }

  public double getPosition(){
      return motorEncoder.getSelectedSensorPosition()/kballsuccerTicksPerInch;
  }

  private void positionUpdater(){
   if(mWantedPosition!=mTravelingPosition){
       mTravelingPosition=mWantedPosition;
       if(!jog)
       jog=false;
       motorEncoder.set(ControlMode.Position, mTravelingPosition*kballsuccerTicksPerInch);
    } 
  }

  private void setLimitClear(boolean e)
  {
    motorEncoder.configClearPositionOnLimitR(e, 0);
  }
}



//System.out.println("sonic0: "+sonic0.getValue()+" sonic1: "+sonic1.getValue());
//330 = 30 inch
//